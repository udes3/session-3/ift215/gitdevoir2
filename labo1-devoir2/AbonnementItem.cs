﻿namespace labo1_devoir2
{
    public partial class Form1
    {
        public class AbonnementItem
        {
            public int ID { get; set; }
            public string Texte { get; set; }
            public double Valeur { get; set; }
        }
    }
}