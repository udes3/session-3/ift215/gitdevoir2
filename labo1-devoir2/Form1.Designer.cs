﻿namespace labo1_devoir2
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.b_show = new System.Windows.Forms.Button();
            this.b_hide = new System.Windows.Forms.Button();
            this.lb_special = new System.Windows.Forms.Label();
            this.lb_chiffre = new System.Windows.Forms.Label();
            this.lb_aide_courriel = new System.Windows.Forms.Label();
            this.lb_min = new System.Windows.Forms.Label();
            this.lb_maj = new System.Windows.Forms.Label();
            this.lb_12char = new System.Windows.Forms.Label();
            this.lb_existe = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.tb_courriel = new System.Windows.Forms.TextBox();
            this.tb_nom = new System.Windows.Forms.TextBox();
            this.tb_prenom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cb_abonnement = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_code = new System.Windows.Forms.TextBox();
            this.lb_code = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonX = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.b_show);
            this.groupBox1.Controls.Add(this.b_hide);
            this.groupBox1.Controls.Add(this.lb_special);
            this.groupBox1.Controls.Add(this.lb_chiffre);
            this.groupBox1.Controls.Add(this.lb_aide_courriel);
            this.groupBox1.Controls.Add(this.lb_min);
            this.groupBox1.Controls.Add(this.lb_maj);
            this.groupBox1.Controls.Add(this.lb_12char);
            this.groupBox1.Controls.Add(this.lb_existe);
            this.groupBox1.Controls.Add(this.tb_password);
            this.groupBox1.Controls.Add(this.tb_courriel);
            this.groupBox1.Controls.Add(this.tb_nom);
            this.groupBox1.Controls.Add(this.tb_prenom);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.groupBox1.Location = new System.Drawing.Point(97, 129);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(619, 491);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Renseignements d\'authentification";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(71, 299);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(198, 15);
            this.label9.TabIndex = 18;
            this.label9.Text = "Optionnel avec un code d\'activation";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(71, 234);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(198, 15);
            this.label8.TabIndex = 17;
            this.label8.Text = "Optionnel avec un code d\'activation";
            // 
            // b_show
            // 
            this.b_show.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b_show.Image = ((System.Drawing.Image)(resources.GetObject("b_show.Image")));
            this.b_show.Location = new System.Drawing.Point(441, 268);
            this.b_show.Name = "b_show";
            this.b_show.Size = new System.Drawing.Size(45, 36);
            this.b_show.TabIndex = 16;
            this.b_show.UseVisualStyleBackColor = true;
            this.b_show.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button2_MouseDown);
            this.b_show.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button2_MouseUp);
            // 
            // b_hide
            // 
            this.b_hide.Cursor = System.Windows.Forms.Cursors.Hand;
            this.b_hide.ForeColor = System.Drawing.SystemColors.Control;
            this.b_hide.Image = ((System.Drawing.Image)(resources.GetObject("b_hide.Image")));
            this.b_hide.Location = new System.Drawing.Point(441, 268);
            this.b_hide.Name = "b_hide";
            this.b_hide.Size = new System.Drawing.Size(45, 36);
            this.b_hide.TabIndex = 15;
            this.b_hide.UseVisualStyleBackColor = true;
            this.b_hide.Visible = false;
            this.b_hide.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button2_MouseDown);
            this.b_hide.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button2_MouseUp);
            // 
            // lb_special
            // 
            this.lb_special.AutoSize = true;
            this.lb_special.ForeColor = System.Drawing.Color.Coral;
            this.lb_special.Location = new System.Drawing.Point(278, 425);
            this.lb_special.Name = "lb_special";
            this.lb_special.Size = new System.Drawing.Size(189, 25);
            this.lb_special.TabIndex = 14;
            this.lb_special.Text = "- 2 caractères spéciaux";
            this.lb_special.ForeColorChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // lb_chiffre
            // 
            this.lb_chiffre.AutoSize = true;
            this.lb_chiffre.ForeColor = System.Drawing.Color.Coral;
            this.lb_chiffre.Location = new System.Drawing.Point(278, 400);
            this.lb_chiffre.Name = "lb_chiffre";
            this.lb_chiffre.Size = new System.Drawing.Size(181, 25);
            this.lb_chiffre.TabIndex = 13;
            this.lb_chiffre.Text = "- Un chiffre minimum";
            this.lb_chiffre.ForeColorChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // lb_aide_courriel
            // 
            this.lb_aide_courriel.AutoSize = true;
            this.lb_aide_courriel.Location = new System.Drawing.Point(286, 243);
            this.lb_aide_courriel.Name = "lb_aide_courriel";
            this.lb_aide_courriel.Size = new System.Drawing.Size(157, 25);
            this.lb_aide_courriel.TabIndex = 9;
            this.lb_aide_courriel.Text = "Adresse incorrecte";
            this.lb_aide_courriel.Visible = false;
            // 
            // lb_min
            // 
            this.lb_min.AutoSize = true;
            this.lb_min.ForeColor = System.Drawing.Color.Coral;
            this.lb_min.Location = new System.Drawing.Point(278, 375);
            this.lb_min.Name = "lb_min";
            this.lb_min.Size = new System.Drawing.Size(220, 25);
            this.lb_min.TabIndex = 12;
            this.lb_min.Text = "- Une minuscule minimum";
            this.lb_min.ForeColorChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // lb_maj
            // 
            this.lb_maj.AutoSize = true;
            this.lb_maj.ForeColor = System.Drawing.Color.Coral;
            this.lb_maj.Location = new System.Drawing.Point(278, 350);
            this.lb_maj.Name = "lb_maj";
            this.lb_maj.Size = new System.Drawing.Size(219, 25);
            this.lb_maj.TabIndex = 11;
            this.lb_maj.Text = "- Une majuscule minimum";
            this.lb_maj.ForeColorChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // lb_12char
            // 
            this.lb_12char.AutoSize = true;
            this.lb_12char.ForeColor = System.Drawing.Color.Coral;
            this.lb_12char.Location = new System.Drawing.Point(278, 325);
            this.lb_12char.Name = "lb_12char";
            this.lb_12char.Size = new System.Drawing.Size(208, 25);
            this.lb_12char.TabIndex = 10;
            this.lb_12char.Text = "- Au moins 12 caractères";
            this.lb_12char.ForeColorChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // lb_existe
            // 
            this.lb_existe.AutoSize = true;
            this.lb_existe.Location = new System.Drawing.Point(286, 243);
            this.lb_existe.Name = "lb_existe";
            this.lb_existe.Size = new System.Drawing.Size(149, 25);
            this.lb_existe.TabIndex = 8;
            this.lb_existe.Text = "Adresse existente";
            this.lb_existe.Visible = false;
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(285, 271);
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '*';
            this.tb_password.Size = new System.Drawing.Size(150, 31);
            this.tb_password.TabIndex = 7;
            this.tb_password.TextChanged += new System.EventHandler(this.PasswordChange);
            this.tb_password.TextChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // tb_courriel
            // 
            this.tb_courriel.Location = new System.Drawing.Point(285, 206);
            this.tb_courriel.Name = "tb_courriel";
            this.tb_courriel.Size = new System.Drawing.Size(150, 31);
            this.tb_courriel.TabIndex = 6;
            this.tb_courriel.TextChanged += new System.EventHandler(this.PasswordChange);
            this.tb_courriel.TextChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // tb_nom
            // 
            this.tb_nom.Location = new System.Drawing.Point(285, 142);
            this.tb_nom.Name = "tb_nom";
            this.tb_nom.Size = new System.Drawing.Size(150, 31);
            this.tb_nom.TabIndex = 5;
            // 
            // tb_prenom
            // 
            this.tb_prenom.Location = new System.Drawing.Point(285, 77);
            this.tb_prenom.Name = "tb_prenom";
            this.tb_prenom.Size = new System.Drawing.Size(150, 31);
            this.tb_prenom.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(149, 274);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 25);
            this.label5.TabIndex = 3;
            this.label5.Text = "Mot de passe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(131, 209);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Adresse courriel";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(217, 145);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Nom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(195, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Prénom";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(191, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(432, 48);
            this.label1.TabIndex = 2;
            this.label1.Text = "Créer un nouveau compte";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.groupBox2.Controls.Add(this.cb_abonnement);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(97, 643);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(619, 114);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Renseignements d\'abonnement";
            // 
            // cb_abonnement
            // 
            this.cb_abonnement.FormattingEnabled = true;
            this.cb_abonnement.Location = new System.Drawing.Point(293, 54);
            this.cb_abonnement.Name = "cb_abonnement";
            this.cb_abonnement.Size = new System.Drawing.Size(182, 33);
            this.cb_abonnement.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(107, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(170, 25);
            this.label6.TabIndex = 0;
            this.label6.Text = "Type d\'abonnement";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.button1.Location = new System.Drawing.Point(604, 831);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(112, 34);
            this.button1.TabIndex = 4;
            this.button1.Text = "Soumettre";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // tb_code
            // 
            this.tb_code.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.tb_code.Location = new System.Drawing.Point(267, 788);
            this.tb_code.Name = "tb_code";
            this.tb_code.Size = new System.Drawing.Size(182, 31);
            this.tb_code.TabIndex = 6;
            this.tb_code.Visible = false;
            this.tb_code.TextChanged += new System.EventHandler(this.groupBox3_Load);
            // 
            // lb_code
            // 
            this.lb_code.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lb_code.AutoSize = true;
            this.lb_code.Location = new System.Drawing.Point(101, 791);
            this.lb_code.Name = "lb_code";
            this.lb_code.Size = new System.Drawing.Size(150, 25);
            this.lb_code.TabIndex = 7;
            this.lb_code.Text = "Code d\'activation";
            this.lb_code.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBox3.Controls.Add(this.buttonX);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(140, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(515, 111);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Visible = false;
            this.groupBox3.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.groupBox3_doubleClick);
            this.groupBox3.MouseLeave += new System.EventHandler(this.groupBox3_Leave);
            this.groupBox3.MouseHover += new System.EventHandler(this.groupBox3_Hover);
            // 
            // buttonX
            // 
            this.buttonX.BackColor = System.Drawing.Color.IndianRed;
            this.buttonX.Location = new System.Drawing.Point(464, 0);
            this.buttonX.Name = "buttonX";
            this.buttonX.Size = new System.Drawing.Size(51, 34);
            this.buttonX.TabIndex = 3;
            this.buttonX.Text = "X";
            this.buttonX.UseVisualStyleBackColor = false;
            this.buttonX.Click += new System.EventHandler(this.buttonX_Click);
            this.buttonX.MouseLeave += new System.EventHandler(this.buttonX_Leave);
            this.buttonX.MouseHover += new System.EventHandler(this.buttonX_Hover);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(179, 71);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(180, 25);
            this.label11.TabIndex = 2;
            this.label11.Text = "Fermeture dans 5 sec";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 37);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(449, 25);
            this.label10.TabIndex = 1;
            this.label10.Text = "Tous les champs requis pour la soumission sont valides";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(97, 760);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(266, 25);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Inscription par code d\'activation";
            this.linkLabel1.Click += new System.EventHandler(this.label7_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(800, 974);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.lb_code);
            this.Controls.Add(this.tb_code);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private GroupBox groupBox1;
        private TextBox tb_password;
        private TextBox tb_courriel;
        private TextBox tb_nom;
        private TextBox tb_prenom;
        private Label label5;
        private Label label4;
        private Label label3;
        private Label label2;
        private Label label1;
        private Label lb_aide_courriel;
        private Label lb_existe;
        private GroupBox groupBox2;
        private ComboBox cb_abonnement;
        private Label label6;
        private Button button1;
        private Label lb_12char;
        private Label lb_special;
        private Label lb_chiffre;
        private Label lb_min;
        private Label lb_maj;
        private Button b_hide;
        private Button b_show;
        private TextBox tb_code;
        private Label lb_code;
        private Label label8;
        private Label label9;
        private GroupBox groupBox3;
        private System.Windows.Forms.Timer timer1;
        private Label label10;
        private Label label11;
        private LinkLabel linkLabel1;
        private Button buttonX;
    }
}