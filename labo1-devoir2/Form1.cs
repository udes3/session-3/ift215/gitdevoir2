//F�lix Nadeau et �tienne Durocher

using System.Reflection.Metadata.Ecma335;
using System.Text.RegularExpressions;

namespace labo1_devoir2
{
    public partial class Form1 : Form
    {

        private List<Compte> comptes = new List<Compte>();
        private static System.Timers.Timer timer;
        private int timeLeft;
        private bool notifyHappened = false;

        public Form1()
        {
            InitializeComponent();
            InitMyComponents();
        }

        private void InitMyComponents()
        {
            cb_abonnement.DataSource = new AbonnementItem[]
            {
                new AbonnementItem{ID = 0, Texte = "Basique" , Valeur = 10.99},
                new AbonnementItem{ID = 1, Texte = "Premium" , Valeur = 50.99},
                new AbonnementItem{ID = 2, Texte = "Entraineur" , Valeur = 100.99},
            };
            cb_abonnement.DisplayMember = "Texte"; cb_abonnement.ValueMember = "ID";
            tb_courriel.TextChanged += CourrielChange;
            CreerCompte();
        }

        public Dictionary<string, bool> IsPasswordValid(string password)
        {
            Dictionary<string, bool> validations = new Dictionary<string, bool>();
            try
            {
                Regex reg1 = new Regex(@"[a-z]+");
                Regex reg2 = new Regex(@"[A-Z]+");
                Regex reg3 = new Regex(@"[0-9]+");
                Regex reg4 = new Regex(@"[@#$%&*;:~]{2,}");
                Regex reg5 = new Regex(@"[A-Za-z0-9@#$%&*;:~]{12,}$");

                if (reg1.IsMatch(password))
                {
                    validations.Add("lowerCase", true);
                }
                else
                {
                    validations.Add("lowerCase", false);
                }

                if (reg2.IsMatch(password))
                {
                    validations.Add("upperCase", true);
                }
                else
                {
                    validations.Add("upperCase", false);
                }

                if (reg3.IsMatch(password))
                {
                    validations.Add("number", true);
                }
                else
                {
                    validations.Add("number", false);
                }

                if (reg4.IsMatch(password))
                {
                    validations.Add("special", true);
                }
                else
                {
                    validations.Add("special", false);
                }

                if (reg5.IsMatch(password))
                {
                    validations.Add("char", true);
                }
                else
                {
                    validations.Add("char", false);
                }
                
                return validations;
            }
            catch
            {
                validations.Add("lowerCase", false);
                validations.Add("upperCase", false);
                validations.Add("number", false);
                validations.Add("special", false);
                validations.Add("char", false);
                return validations;
            }
        }

        public bool IsEmailAddressValid(string email)
        {
            try
            {
                Regex reg = new Regex(@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$",
                RegexOptions.IgnoreCase);
                return (reg.IsMatch(email));
            }
            catch
            {
                return false;
            }
        }

        public bool isActivationCodeValid(string code)
        {
            try
            {
                Regex reg = new Regex(@"^[0-9]{8}$");
                return (reg.IsMatch(code));
            }
            catch
            {
                return false;
            }
        }

        private void CreerCompte()
        {
            comptes = new List<Compte>();
            comptes.Add(new Compte
            {
                Prenom = "John",
                Nom = "Wick",
                Courriel = "donotkill@my.dog",
                MotDePasse = "asdfsdfsdf",
                Abonnement = 0
            });
            comptes.Add(new Compte
            {
                Prenom = "John",
                Nom = "Malchovick",
                Courriel = "being@him.com",
                MotDePasse = "asdfsdfsdf",
                Abonnement = 1
            });
            comptes.Add(new Compte
            {
                Prenom = "Alvine",
                Nom = "Herman",
                Courriel = "courriel@serveur.domaine",
                MotDePasse = "asdfsdfsdf",
                Abonnement = 0
            });
        }

        private void CourrielChange(object sender, EventArgs e)
        {
            string email = tb_courriel.Text;
            if (IsEmailAddressValid(email))
            {
                var compteExistant = comptes.FirstOrDefault(o => o.Courriel == email);
                if (compteExistant != null)
                {
                    tb_courriel.BackColor = Color.Coral;
                    lb_existe.Visible = true;
                    lb_existe.ForeColor = Color.Coral;
                }
                else
                {
                    tb_courriel.BackColor = Color.Aquamarine;
                    lb_aide_courriel.Visible = false;
                    lb_existe.Visible = false;
                }
            }
            else
            {
                tb_courriel.BackColor = Color.Coral;
                lb_aide_courriel.Visible = true;
                lb_aide_courriel.ForeColor = Color.Coral;
            }
        }

        private void PasswordChange(object sender, EventArgs e)
        {
            string password = tb_password.Text;
            Dictionary<string, bool> validations = IsPasswordValid(password);

            if (validations.GetValueOrDefault("char"))
            {
                lb_12char.ForeColor = Color.Green;
            }
            else
            {
                lb_12char.ForeColor = Color.Coral;
            }

            if (validations.GetValueOrDefault("upperCase"))
            {
                lb_maj.ForeColor = Color.Green;
            }
            else
            {
                lb_maj.ForeColor = Color.Coral;
            }

            if (validations.GetValueOrDefault("lowerCase"))
            {
                lb_min.ForeColor = Color.Green;
            }
            else
            {
                lb_min.ForeColor = Color.Coral;
            }

            if (validations.GetValueOrDefault("number"))
            {
                lb_chiffre.ForeColor = Color.Green;
            }
            else
            {
                lb_chiffre.ForeColor = Color.Coral;
            }

            if (validations.GetValueOrDefault("special"))
            {
                lb_special.ForeColor = Color.Green;
            }
            else
            {
                lb_special.ForeColor = Color.Coral;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            string prenom = tb_prenom.Text;
            string nom = tb_nom.Text;
            string courriel = tb_courriel.Text;
            string motDePasse = tb_password.Text;
            string codeActivation = tb_code.Text;

            if (((courriel == "" || motDePasse == "") && codeActivation == ""))
            {
                MessageBox.Show("Tous les champs doivent �tre rempli");
            }
            else
            {
                AbonnementItem abonnement = (AbonnementItem)cb_abonnement.SelectedItem;
                string resume = prenom + " " + nom + " " + courriel + " " + abonnement.Texte
                + " � " + abonnement.Valeur + "$.";
            
                MessageBox.Show(resume);
                Compte nouveau = new Compte
                {
                    Prenom = prenom,
                    Nom = nom,
                    Courriel = courriel,
                    MotDePasse = motDePasse,
                    Abonnement = abonnement.ID
                };
                comptes.Add(nouveau);
            }
            
        }

        private void button2_MouseDown(object sender, MouseEventArgs e)
        {
            tb_password.PasswordChar = '\0';
            b_hide.Visible = true;
            b_show.Visible = false;
        }
        private void button2_MouseUp(object sender, MouseEventArgs e)
        {
            tb_password.PasswordChar = '*';
            b_hide.Visible = false;
            b_show.Visible = true;
        }

        private void label7_Click(object sender, EventArgs e)
        {
            bool clicked = tb_code.Visible;

            if (clicked)
            {
                lb_code.Visible = false;
                tb_code.Visible = false;
                return;
            }
            lb_code.Visible = true;
            tb_code.Visible = true;
        }

        private async void groupBox3_Load(object sender, EventArgs e)
        {
            string password = tb_password.Text;
            string email = tb_courriel.Text;
            string code = tb_code.Text;
            

            Dictionary<string, bool> validPassword = IsPasswordValid(password);
            bool validEmail = IsEmailAddressValid(email);
            bool codeValid = isActivationCodeValid(code);

            if ((validPassword.ContainsValue(false) || !validEmail) && code =="")
            {
                return;
            }

            Timer();
            

        }

        private async void Timer()
        {
            timeLeft = 5;

            timer1.Start();
            timer1.Interval = 1000;

            notifyHappened = true;
        }

        void timer1_Tick(object sender, EventArgs e)
        {
            label11.Text = "Fermeture dans " + timeLeft + " sec";

            if (timeLeft != 0)
            {
                groupBox3.Visible = true;
                timeLeft--;
            }
            else
            {
                timer1.Stop();
                label11.Text = "Fermeture dans 5 sec";
                timeLeft = 5;
                groupBox3.Visible = false;
            }
        }

        private void groupBox3_Hover(object sender, EventArgs e)
        {
            timer1.Stop();
        }
        private void groupBox3_Leave(object sender, EventArgs e)
        {
            timer1.Start();
        }
        private void groupBox3_doubleClick(object sender, MouseEventArgs e)
        {
            groupBox3.Visible = false;
        }

        private void buttonX_Hover(object sender, EventArgs e)
        {
            timer1.Stop();
            buttonX.BackColor = Color.Red;
        }
        private void buttonX_Leave(object sender, EventArgs e)
        {
            buttonX.BackColor = Color.IndianRed;
        }

        private void buttonX_Click(object sender, EventArgs e)
        {
            groupBox3.Visible = false;
        }
    }
}